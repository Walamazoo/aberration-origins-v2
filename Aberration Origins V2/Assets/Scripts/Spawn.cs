﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code made based of code from https://noobtuts.com/unity/tower-defense-game

public class Spawn : MonoBehaviour
{
    // The Monster that should be spawned
    public GameObject monsterPrefab;

    // Spawn interval in seconds
    public float interval = 3;

     // Spawn delay in seconds
    public float delay = 5;

    // Start is called before the first frame update
    void Start() {
        InvokeRepeating("SpawnNext", delay, interval);
    }

    void SpawnNext() {
        Instantiate(monsterPrefab, transform.position, Quaternion.identity);
    }

}
