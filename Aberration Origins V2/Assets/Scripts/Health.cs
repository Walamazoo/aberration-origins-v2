﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code made based of code from https://noobtuts.com/unity/tower-defense-game

public class Health : MonoBehaviour
{
    public ResourceController rc;

    // The TextMesh Component
    TextMesh tm;

    // Use this for initialization
    void Start () {
        tm = GetComponent<TextMesh>();
        rc = FindObjectsOfType<ResourceController>()[0];
    }

    // Update is called once per frame
    void Update () {
        // Face the Camera
        transform.forward = Camera.main.transform.forward;
    }

    // Return the current Health by counting the '-'
    public int current() {
        return tm.text.Length;
    }

    // Decrease the current Health by removing one '-'
    public void decrease() {
        if (current() > 1) {
            tm.text = tm.text.Remove(tm.text.Length - 1);
        }
        else{
            if(transform.parent.gameObject.GetComponent<Monster>()){
                if(transform.parent.gameObject.GetComponent<BossMonster>()){
                    rc.addBossResources();
                }
                else{
                    rc.addResources();
                }
            }
            Destroy(transform.parent.gameObject);
        }
    }
}
