﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code made based of code from https://noobtuts.com/unity/tower-defense-game

public class Monster : MonoBehaviour
{
    public float enemySpeed=1;

    // Start is called before the first frame update
    void Start() {
        // Navigate to Castle
        GameObject castle = GameObject.Find("Castle");
        if (castle){
            UnityEngine.AI.NavMeshAgent agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            agent.destination = castle.transform.position;
            agent.speed=enemySpeed;
        }
    }

    void OnTriggerEnter(Collider co) {
        // If castle then deal Damage, destroy self
        if (co.name == "Castle") {
            co.GetComponentInChildren<Health>().decrease();
            Destroy(gameObject);
        }
    }

}
