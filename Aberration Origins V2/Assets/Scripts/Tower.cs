﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code made based of code from https://noobtuts.com/unity/tower-defense-game and https://forum.unity.com/threads/ai-that-fires-when-the-closest-enemy-is-in-range.572542/

public class Tower : MonoBehaviour
{
    // The Bullet
    public GameObject bulletPrefab;

    // Rotation Speed
    public float rotationSpeed = 35;

    public float range = 5;

    public float fireRate = 1;

    // Update is called once per frame
    void Update() {
        transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed, Space.World);
    }

    void Start(){
        InvokeRepeating("Fire", 0, fireRate);
    }

    void Fire(){
        GameObject[] allTargets=GameObject.FindGameObjectsWithTag("Enemy");
        if (allTargets != null){
            GameObject closest = allTargets[0];
            foreach (GameObject tmpTarget in allTargets)
            {
                if (Vector3.Distance(transform.position, tmpTarget.transform.position) < Vector3.Distance(transform.position, closest.transform.position))
                {
                    closest = tmpTarget;
                }
            }
            if (Vector3.Distance(transform.position, closest.transform.position) < range)
            {
                GameObject g = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                g.GetComponent<Bullet>().target = closest.transform;
            }
        }
    }

}
