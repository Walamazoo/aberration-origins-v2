﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

//Code for timer made with help from https://stackoverflow.com/questions/30056471/how-to-make-the-script-wait-sleep-in-a-simple-way-in-unity
//TODO: Create lose sequence

public class GameController : MonoBehaviour
{
    public GameObject spawns;
    public GameObject buildplaces; 
    public GameObject loseText;
    public GameObject introText;
    float timer=0;
    float timer2=0;
    bool timerReached=false;
    bool timerReached2=true;

    // Start is called before the first frame update
    void Start()
    {
        introText.SetActive(true);
        loseText.SetActive(false);
        spawns.SetActive(false);
        buildplaces.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!timerReached){
            timer+=Time.deltaTime;
        }
        if(!timerReached2){
            timer2+=Time.deltaTime;
        }
        if(!timerReached&&timer>8){
            timerReached=true;
            introText.SetActive(false);
            spawns.SetActive(true);
            buildplaces.SetActive(true);
        }
        if(!timerReached2&&timer2>5){
            timerReached2=true;
            SceneManager.LoadScene(0);
        }
        GameObject[] allBases=GameObject.FindGameObjectsWithTag("Base");
        if(allBases.Length==0){
            loseText.SetActive(true);
            timerReached2=false;
        }
    }
}
