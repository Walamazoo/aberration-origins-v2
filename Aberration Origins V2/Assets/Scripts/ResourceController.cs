﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceController : MonoBehaviour
{
    public TextMeshProUGUI resourceText;

    public int resources = 10;

    // Start is called before the first frame update
    void Start()
    {
        SetResourceText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        SetResourceText();
    }

    void SetResourceText()
    {
        resourceText.text = "Resources: " + resources.ToString();
    }

    public int getResources(){
        return resources;
    }

    public void addResources(){
        resources = resources+1;
    }

    public void addBossResources(){
        resources = resources+5;
    }

    public void subtractTowerResources(){
        resources = resources-5;
    }

    public void subtractUpgradeResources(){
        resources = resources-8;
    }
}
