﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Code made based of code from https://noobtuts.com/unity/tower-defense-game

public class Buildplace : MonoBehaviour
{
    public ResourceController rc;

    // The Tower that should be built
    public GameObject towerPrefab;
    public GameObject upgradePrefab;
    public GameObject tower1;
    public GameObject tower2;

    bool built = false;
    bool levelTwo = false;

    void OnMouseUpAsButton() {
        // Build Tower above Buildplace
        if(built==false&&levelTwo==false&&rc.getResources()>=5){
            rc.subtractTowerResources();
            tower1 = (GameObject)Instantiate(towerPrefab);
            tower1.transform.position = transform.position + Vector3.up/2;
            built=true;
        }
        else if(built==true&&levelTwo==false&&rc.getResources()>=8){
            rc.subtractUpgradeResources();
            Destroy(tower1);
            tower2 = (GameObject)Instantiate(upgradePrefab);
            tower2.transform.position = transform.position + Vector3.up/2;
            levelTwo=true;
        }
    }

}
